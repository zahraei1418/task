import React , {lazy ,Suspense} from 'react';
import {Switch,Route} from 'react-router-dom';

import Spinner from './components/spinner/spinner.component';
import Navigation from './components/navigation/navigation.component';

const HomePage = lazy(() => import('./Pages/HomePage/homePage.component'));
const SignIn = lazy( ()=> import('./components/signIn/signin.component'));
const GetData = lazy( ()=> import('./Pages/GetData/GatData.component'));

const App = () => {
  return (
    <div>
      <Navigation />
        <Switch>
          <Suspense fallback={<Spinner/>}>
            <Route exact path='/' component={HomePage}/>
            <Route exact path='/getData' component={GetData} />
            <Route exact path='/signin' component={SignIn}/>
          </Suspense>
        </Switch>
    </div>
  );
}

export default App;