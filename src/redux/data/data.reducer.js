import DataActionTypes from './data.types';

const INITIAL={
    data:null,
    // isFetching:false,
    errorMessage:undefined
}

const DataReducer = (state=INITIAL , action)=>{
    switch(action.type){
        // case DataActionTypes.FETCH_COLLECTION_START:
        //     return {
        //         ...state,
        //         isFetching:true,
        //     }
        case DataActionTypes.FETCH_COLLECTION_SECCUSS:
            return {
                ...state,
                data:action.payload
            }
        case DataActionTypes.FETCH_COLLECTION_FAILURE:
            return {
                ...state,
                // isFetching:false,
                errorMessage:action.payload
            }
        default:
            return state;
    }
};

export default DataReducer;