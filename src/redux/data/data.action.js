import dataActionTypes from './data.types';
import axios from 'axios';

// export const fetchCollectionsStart = () =>({
//    type:dataActionTypes.FETCH_COLLECTION_START
// });

export const fectchCollectionsCuccess = collection =>({
    type:dataActionTypes.FETCH_COLLECTION_SECCUSS,
    payload:collection
});

export const fectchCollectionsFailure = errorMessage =>({
    type:dataActionTypes.fectchCollectionsFailure,
    payload:errorMessage
});

export const fetchCollectionsStartAsync = () =>{
    return dispatch=> {
        // dispatch(fetchCollectionsStart());
        axios
        .get(`https://cors-anywhere.herokuapp.com/http://task.asanshahr.com`)
        .then(res => {
            dispatch(fectchCollectionsCuccess(res.data.result));
        })
        .catch(err => {
            dispatch(fectchCollectionsFailure(err.message));
        });
    }
 }