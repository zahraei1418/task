import {combineReducers} from 'redux';
import DataReducer from './data/data.reducer';

export default combineReducers({
    data:DataReducer
})