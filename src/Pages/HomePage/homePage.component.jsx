import React from 'react';

import './homePage.styles.scss';
import Gallery from '../../components/gallery/gallery.component';

const HomePage = () =>(
    <div className='HomePage'>
        <Gallery/>
    </div>
);

export default HomePage;