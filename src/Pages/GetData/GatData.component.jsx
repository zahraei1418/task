import React from 'react';
// import axios from 'axios';
import './GetData.styles.scss';
/////////////////////////////
import { createStructuredSelector } from 'reselect';
import {selectCollections} from '../../redux/data/data.selectors';
import { fetchCollectionsStartAsync } from '../../redux/data/data.action';
import { connect} from 'react-redux';

import Card from '../../components/card/card.component';

class GetData extends React.Component{
    // constructor(){
    //     super();
    //     this.state={
    //         data:[]
    //     }
    // }

    componentDidMount(){
        const {fetchCollectionsStartAsync} = this.props;
        fetchCollectionsStartAsync();
    // axios.get('http://cors-anywhere.herokuapp.com/http://task.asanshahr.com')
    // axios.get('https://cors-anywhere.herokuapp.com/http://task.asanshahr.com')
    //     .then(res => {
    //     // const data = res.data;
    //     console.log(res.data);
    //     // this.setState({ data });
    //     })
    //     .catch(error=>{
    //         console.log(error);
    //     })
    }

    render(){
        return(
            <div className='getDate'>
                {
                    this.props.data ?
                    this.props.data.map(inf =>(
                        <Card data={inf} key={inf.id} />
                    ))
                    :
                    <h1>kdsj</h1>
                }
                {/* {this.props.data.map(inf=>(
                    <Card data={inf}/>
                ))} */}

            </div>
        )
    }
}

const mapStateToProps =createStructuredSelector({
    data:selectCollections
})

const mapDispatchToProps = dispatch =>({
    fetchCollectionsStartAsync : () => dispatch(fetchCollectionsStartAsync())
})
export default connect(mapStateToProps,mapDispatchToProps)(GetData);
// export default connect(null,mapDispatchToProps)(GetData);