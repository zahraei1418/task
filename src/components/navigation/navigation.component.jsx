import React from 'react';
import {Link ,Router} from 'react-router-dom';
import './navigation.styles.scss';

const Navigation = () =>(
    <div className='header'>
        {/* <Router> */}
        <Link to='/'> 
            <img src={require('../../img/logo.png')} className='header_logo' />
        </Link>
        <div className='header__options'>
            <Link to='/getData' className='header__option'>GetData</Link>
            <Link to='/signin' className='header__option'>sign in</Link>
        </div>
        {/* </Router> */}
    </div>
);

export default Navigation;