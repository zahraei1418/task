import React from 'react';

import './gallery.styles.scss';

const Gallery = () =>(
    <div className='container'>
        <div class="gallery">
            <figure class="gallery__item gallery__item--1"><img src={require('../../img/home1.jpg')} alt="Gallery image 1" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--2"><img src={require('../../img/home17.jpg')} alt="Gallery image 2" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--3"><img src={require('../../img/home2.jpg')} alt="Gallery image 3" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--4"><img src={require('../../img/home4.jpg')} alt="Gallery image 4" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--5"><img src={require('../../img/home12.jpg')} alt="Gallery image 5" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--6"><img src={require('../../img/home6.jpg')} alt="Gallery image 6" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--7"><img src={require('../../img/home16.jpg')} alt="Gallery image 7" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--8"><img src={require('../../img/home8.jpg')} alt="Gallery image 8" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--9"><img src={require('../../img/home15.jpg')} alt="Gallery image 9" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--10"><img src={require('../../img/home10.jpg')} alt="Gallery image 10" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--11"><img src={require('../../img/home11.jpg')} alt="Gallery image 11" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--12"><img src={require('../../img/home1.jpg')} alt="Gallery image 12" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--13"><img src={require('../../img/home14.jpg')} alt="Gallery image 13" class="gallery__img" /></figure>
            <figure class="gallery__item gallery__item--14"><img src={require('../../img/home7.jpg')} alt="Gallery image 14" class="gallery__img" /></figure>
        </div>
    </div>
   
);

export default Gallery;