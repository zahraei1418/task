import React from 'react';
import './card.styles.scss';

const Card = ({data}) =>{
    return(
    <div className='card'>
        <p>area: {data.area}</p>
        <p>blockEdit: {data.blockEdit}</p>
        <p>buildingName: {data.buildingName}</p>
        <p>description: {data.description}</p>
        <p>estateType.name: {data.estateType.name}</p>
    </div>
);}

export default Card;