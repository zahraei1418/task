import React from 'react';
import './customebutton.styles.scss';

const CustomButton=({children,color,...otherProps})=>(
    <button className={`customButton customButton-${color}`} {...otherProps}>{children}</button>
);

export default CustomButton;